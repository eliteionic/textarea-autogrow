import { Directive, Renderer } from '@angular/core';
import { DomController } from 'ionic-angular';

@Directive({
  	selector: '[my-autogrow]',
	host: {
		'(input)' : 'resize($event.target)'
	}
})
export class MyAutogrowDirective {

	constructor(private renderer: Renderer, private domCtrl: DomController) {

	}

	resize(textarea) {

		let newHeight;

		this.domCtrl.read(() => {
			newHeight = textarea.scrollHeight;
		});

		this.domCtrl.write(() => {
			this.renderer.setElementStyle(textarea, 'height', newHeight + 'px');
		});

	}

}
