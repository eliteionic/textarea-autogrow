import { NgModule } from '@angular/core';
import { MyAutogrowDirective } from './my-autogrow/my-autogrow';
@NgModule({
	declarations: [MyAutogrowDirective],
	imports: [],
	exports: [MyAutogrowDirective]
})
export class DirectivesModule {}
